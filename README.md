Info za VirtualHost!


<VirtualHost *:80>
        DocumentRoot "/var/www/ParcijalniIspit"
        ServerName parcijalnispit.loc
        DirectoryIndex index.html

        <Directory "/var/www/ParcijalniIspit">
                Options All
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
